
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const Cognito = new AWS.CognitoIdentityServiceProvider();
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

var SUFFIX = process.env.SUFFIX || '-matt';


exports.getUserOrg = (tkn) => {

  return new Promise((resolve, reject) => {
    if (tkn === null) {
      reject({code: 401, msg: 'AccessToken must be present in request Authorization header'});
    }

    // Cognito.getUser({AccessToken: tkn}, (cErr, cData) => {
    //     if (cErr) {
    //         reject({code: cErr.statusCode ? cErr.statusCode : 401, msg: cErr.message ? cErr.message : 'AccessToken is expired or invalid'});
    //     }
    //     if (!cUser || !cUser.UserAttributes) {
    //         reject({code: 401, msg: 'User not found in user pool'});
    //     }
    //     let email = cUser.UserAttributes.find(a => a.Name === 'email').Value;
    //     Dynamo.getItem({
    //       TableName: 'Users' + SUFFIX,
    //       Key: {
    //         email: email
    //       }
    //   }).promise()
    //     .then(user => {
    //
    //     })
    // });

    Cognito.getUser({AccessToken: tkn}).promise()
      .then(cUser => {
        if (!cUser || !cUser.UserAttributes) {
          reject({code: 401, msg: 'User not found in user pool'});
        }
        let email = cUser.UserAttributes.find(a => a.Name === 'email').Value;
        Dynamo.getItem({
          TableName: 'Users' + SUFFIX,
          Key: {
            email: email
          }
        }).promise()
          .then(user => {
            if (!user || !user.Item || !user.Item.email) {
              reject({code: 401, msg: 'User not found in database'});
            } else {
              Dynamo.getItem({
                TableName: 'Organizations' + SUFFIX,
                Key: {
                  name: user.Item.org
                }
              }).promise()
                .then(org => {
                  if (!org || !org.Item || !org.Item.name) {
                    reject({code: 401, msg: 'Organization not found in database'});
                  } else {
                    resolve({user: user.Item, org: org.Item});
                  }
                });
            }
          }).catch(err => {
            reject({code: 401, msg: err.message ? err.message : 'User/org not valid'});
          });
      }).catch(cErr => {
          reject({code: cErr.statusCode ? cErr.statusCode : 401, msg: cErr.message ? cErr.message : 'AccessToken is invalid or expired'});
      });
  });

};

exports.getEntityByProp = (table, prop, val) => {
  let params = {
    TableName: table + SUFFIX,
    Key: {}
  }
  params.Key[prop] = val;
  Dynamo.getItem(params).promise()
    .then(result => {
      if (!result.Item || !result.Item[prop]) {
        return Promise.reject(`${table.substr(0, table.length - 2)} not found in database`);
      } else {
        return Promise.resolve(data.Item)
      }
    });
};

exports.parseFromJson = (item, prop) => {
  if (item[prop]) {
    return item;
  } else if (JSON.parse(item)[prop]) {
    return JSON.parse(item);
  } else {
    return 'Request body must be present and in valid JSON format';
  }
}
