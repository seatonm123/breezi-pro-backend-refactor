const Core = require('./core');
// local dependency -- will be added to Lambda Layer soon

const AWS = require('aws-sdk');
AWS.config.update({
  region: 'eu-west-1'
});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();
// remote dependencies, will also be added to consolidated Lambda Layer

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': process.env.ORIGIN || '*'
      }
    });
  };
  // Boilerplate Nodejs Lambda stuff


  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be present in request Authorization header');
  }
  // check that jwt is present in req header

  if (event.httpMethod !== 'POST') {
    response(405, 'HTTP method not allowed');
  }
  // This service is ONLY for POSTing at the moment, since we have exchanged individual GET calls with the bundle service

  Core.getUserOrg(event.headers.Authorization)

    // This method will authorize and authenticate the user

    .then(details => {
      const admin = details.user;
      const org = details.org;

      // These properties aren't obligatory here but may be needed in the future and are needed for other services that use the same (CORE) methods

      let json = parseJSONArray(event.body, 'did');
      if (!Array.isArray(json)) {
        response(403, json);
      }
      // JSON validation
      postByBuilding(json);
    }, err => {
      response(err.code, err.msg);
      // Error response from ./core.js
    });

  function parseJSONArray(items) {
    let arr;
    if (Array.isArray(items)) {
      arr = items;
    } else if (Array.isArray(JSON.parse(items))) {
      arr = JSON.parse(items);
    }
    try {
      if (arr.every(hasProp)) {
        return arr;
      } else {
        return 'At least one measurement does not contain a valid deviceId'
      }
    } catch (err) {
      return 'Request body must be in valid JSON format';
    }

    function hasProp(a) {
      return a.did;
    }

  }

  function postByBuilding(measurements) {
    let byDevice = measurements.reduce((a, i) => {
      let devObj = a.find(d => d.did === i.did);
      if (devObj === undefined) {
        a.push({
          did: i.did,
          measurements: [i]
        });
      } else {
        devObj.measurements.push(i);
      }
      return a;
    }, []);

    // The above logic accumulates the measurements into the following structure (object): {did: deviceId, measurements: [measArray]} so that we can match device to building quickly

    Promise.all(byDevice.map(b => {
      return new Promise(resolve => {
        getDev(b)

        // Here the service gets the device info (including airVentAssignment and bId) from the DB

          .then(device => {
            getBldgInfo(device)

            // Here the building info is gathered in order to assign the current airFilterId to the measurement(s) to be POSTed

              .then(dBldg => {
                Promise.all(dBldg.measurements.map(m => {

                    // With all necessary data accumulated to POST, measurements (including airFilterId) are now POSTed

                  return new Promise(resolve => {
                    postMeas(m)
                      .then(posted => {
                        resolve(true)
                      }, postErr => {
                        resolve(false);
                      });
                  });
                })).then(final => {
                  let successfulFinals = final.filter(f => f !== false);
                  if (successfulFinals.length > 0) {

                      // Aggregated success/fail response returned from service (and logged in CloudWatch)

                    resolve({
                      success: true,
                      device: b.did,
                      attempted: final.length,
                      successfullyPosted: successfulFinals.length
                    });
                  }
                });
              }, dBldgErr => {
                console.log({
                  FAILED: b.did,
                  ERROR: dBldgErr
                });
                resolve({
                  success: false,
                  device: b.did,
                  step: 'building',
                  err: dBldgErr
                });
              });
          }, devErr => {
            console.log({
              FAILED: b.did,
              ERROR: devErr
            });
            resolve({
              success: false,
              device: b.did,
              step: 'device',
              err: devErr
            });
          });
      });
    })).then(result => {
        console.log(JSON.stringify(result, null, 1));
      response(200, result);
    });
  }

  function getDev(dev) {
    return new Promise((resolve, reject) => {
      Dynamo.getItem({
        TableName: 'Devices-matt',
        Key: {
          deviceId: dev.did
        }
      }, (err, data) => {
        if (err) {
          reject(err);
        } else {
          if (!data.Item || !data.Item.airVentAssignments) {
            reject('device not assigned');
          }
          let device = data.Item;
          try {
            let currentAssignment = device.airVentAssignments.find(v => !v.toTimestamp);
            if (currentAssignment === undefined || !currentAssignment.buildingId) {
              reject('no current airVentAssignment');
            }
            dev.bId = currentAssignment.buildingId;
            dev.vId = currentAssignment.airVentId;
            dev.assignedTms = currentAssignment.fromTimestamp;

            // device object now has this structure (if successful): {did: deviceId, bId: buildingId, vId: ventId, assignedTms: tmsDeviceWasInstalledInVent}

            resolve(dev);
          } catch (err) {
            reject('device does not exist');
          }
        }
      });
    })
  }

  function getBldgInfo(dev) {
    return new Promise((resolve, reject) => {
      Dynamo.getItem({
        TableName: 'Buildings-matt',
        Key: {
          id: dev.bId
        }
      }, (err, data) => {
        if (err) {
          reject(err);
        } else {
          if (!data.Item || !data.Item.airVents) {
            reject('no building or vents');
          }
          let bldg = data.Item;
          let vent = bldg.airVents.find(v => v.id === dev.vId);
          if (vent === undefined) {
            reject('vent does not exist');
          }
          let filter = vent.airFilters.find(f => !f.toTimestamp);
          if (filter === undefined) {
            reject('no active filter');
          }
          dev.measurements.forEach(m => {
            m.airFilterId = filter.id;
          });

          // device object not changed, but all measurements now contain valid, current airFilterId

          resolve(dev);
        }
      });
    });
  }

  function postMeas(meas) {
    return new Promise((resolve, reject) => {
      Dynamo.putItem({
        TableName: 'Measurements-matt',
        Item: meas
      }, (err, data) => {
        if (err) {
          console.log({
            ERROR: err
          });
          reject(null);
        } else {
          resolve(true);
        }
      });
    });
  }

};
