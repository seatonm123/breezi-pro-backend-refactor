
const fs = require('fs');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

async function writeBldgs(){
    let bldgs = await Dynamo.scan({TableName: 'Buildings-matt'}).promise();
    fs.writeFile('./bldgs.json', JSON.stringify(bldgs.Items, null, 1), (err, res) => {
        err ? console.log({ERROR: err}) : console.log('Successfully wrote buildings');;
    });
}

async function writeDevs(){
    let devs = await Dynamo.scan({TableName: 'Devices-matt'}).promise();
    fs.writeFile('./devs.json', JSON.stringify(devs.Items, null, 1), (err, data) => {
        err ? console.log({ERROR: err}) : console.log('Successfully wrote devices');;
    });
}

// writeBldgs();
// writeDevs();

let bldgs = require('./bldgs');
let devs = require('./devs');

function assignBIds(){
    let devices = bldgs.reduce((a, i) => {
        if (i.airVents) {
            i.airVents.forEach(v => {
                let vDev = devs.find(d => d.deviceId === v.device);
                if (vDev !== undefined) {
                    let currentVent = vDev.airVentAssignments.find(s => !s.toTimestamp);
                    if (currentVent !== undefined) {
                        currentVent.buildingId = i.id;
                    }
                }
                a.push(vDev);
            });
        }
        return a;
    }, []);
    Promise.all(devices.forEach(d => {
        console.log(d.deviceId);
        let params = {
            TableName: 'Devices-matt',
            Key: {
                deviceId: d.deviceId
            },
            UpdateExpression: 'set airVentAssignments = :airVentAssignments',
            ExpressionAttributeValues: {
                ':airVentAssignments': d.airVentAssignments
            }
        };
        Dynamo.updateItem(params, (err, data) => {
            if (err) {
                console.log({ERROR: err});
            } else {
                console.log('Successfully updated device ' + d.deviceId);
            }
        })
    }));
}

assignBIds();
