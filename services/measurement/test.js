
const Index = require('./index');
const DataSet = require('./testData');

const tkn =
    `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJkYjczZTUwYS02ZmQ4LTRlYzAtYjVlMi01MTcxMTY2MDg3ODMiLCJldmVudF9pZCI6IjIxY2E2YWQ2LTAxOTItNGZiYy05ZThlLTU5YWVlYmY5OTdkMiIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1NzU4NjA0NzEsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU3NTg2NDA3MSwiaWF0IjoxNTc1ODYwNDcxLCJqdGkiOiI4YTIyM2EyZS0zMDI0LTQ3ZjYtYjM5Ny1jNjk2MGExNWVhOGIiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiJkYjczZTUwYS02ZmQ4LTRlYzAtYjVlMi01MTcxMTY2MDg3ODMifQ.aTHq90Vmh_-0Y_t7tEa93strzO_HJtqlSrCLd2dfm9yimjxPUJc1ppybuMSwpnlTmBjfJT0niHMElz_mTuMokd1U3qv-6ZrmdybOII1sTHCPQtjl51FpWpHEwn5osEVPguJgoOQTRlwu_GJQisWsuwES2x22B2cBqQczaia9rMFO2_1xlYgxTfdy1vuiq1BFz1A4q5n-X-T5M03KZzJEGHqEvJdZzp8f3JxVjoNcxnDlJ8Q25n3_Yx308y5X9Y7i4y_vHQGWoTEq_gZsyfom2_PFTRYffqQHbebZ2gsddjA4S3NrGiJkFdEstySILkn9CH_F4mNdfq80DCeThy8iAA`;

const TestEvent = {
    httpMethod: 'POST',
    headers: {
        'Content-Type': 'application/json',
        Authorization: tkn
    },
    body: DataSet
};

Index.handler(TestEvent, {}, (err, res) => {
    if (err) {
        console.log({ERROR: err});
    } else {
        console.log(res);
    }
});
